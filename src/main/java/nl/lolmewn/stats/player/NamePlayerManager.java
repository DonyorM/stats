package nl.lolmewn.stats.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.StatsPlayerLoadedEvent;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.signs.SignType;
import nl.lolmewn.stats.signs.StatsSign;
import org.bukkit.OfflinePlayer;

/**
 *
 * @author Lolmewn
 */
public class NamePlayerManager implements PlayerManager {

    private final Main plugin;
    private final ConcurrentHashMap<String, StatsPlayer> players = new ConcurrentHashMap<String, StatsPlayer>();

    public NamePlayerManager(Main m) {
        plugin = m;
    }

    @Override
    public void addPlayer(OfflinePlayer player, StatsPlayer statsPlayer) {
        this.players.put(player.getName(), statsPlayer);
    }

    @Override
    public StatsPlayer findPlayer(String key) {
        for (StatsPlayer player : this.getPlayers()) {
            if (player.getPlayername().toLowerCase().startsWith(key.toLowerCase())) {
                return player;
            }
        }
        return null;
    }

    @Override
    public StatsPlayer getPlayer(OfflinePlayer player) {
        if (!this.hasPlayer(player)) {
            StatsPlayer sp = new StatsPlayer(plugin, player.getName(), true);
            this.players.put(player.getName(), sp);
            this.loadPlayer(player);
            return sp;
        }
        return players.get(player.getName());
    }

    @Override
    public Collection<StatsPlayer> getPlayers() {
        return this.players.values();
    }

    @Override
    public boolean hasPlayer(OfflinePlayer player) {
        return this.players.containsKey(player.getName());
    }

    @Override
    public void loadPlayer(final OfflinePlayer player) {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                StatsPlayer sp = new StatsPlayer(plugin, player.getName(), false);
                if (plugin.newConfig || plugin.getMySQL() == null) {
                    return;
                }
                plugin.debug("Starting to load " + player.getName() + " async...");
                try {
                    Connection con = plugin.getMySQL().getConnection();
                    PreparedStatement st = con.prepareStatement("SELECT * FROM " + plugin.getSettings().getDbPrefix() + "players WHERE name=? LIMIT 1"); //Change to UUID when 1.7 comes out
                    st.setString(1, player.getName());
                    ResultSet set = st.executeQuery();
                    if (!set.next()) {
                        //There's no player yet!
                        PreparedStatement insert = con.prepareStatement("INSERT INTO " + plugin.getSettings().getDbPrefix() + "players (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
                        insert.setString(1, player.getName());
                        int affected = insert.executeUpdate();
                        if (affected == 0) {
                            throw new SQLException("No user exists, but a new can't be created either. :O");
                        }
                        ResultSet keys = insert.getGeneratedKeys();
                        if (keys.next()) {
                            sp.setId(keys.getInt(1));
                        }
                        keys.close();
                        insert.close();
                    } else {
                        sp.setId(set.getInt("player_id"));
                    }
                    set.close();
                    st.close();
                    for (StatsTable table : plugin.getStatsTableManager().values()) {
                        table.loadStats(con, sp);
                    }
                    con.close();
                } catch (SQLException e) {
                    Logger.getLogger(UUIDPlayerManager.class.getName()).log(Level.SEVERE, null, e);
                }

                if (players.containsKey(player.getName())) {
                    StatsPlayer old = players.get(player.getName());
                    boolean has = false;
                    for (String world : old.getWorlds()) {
                        if (old.hasStat(plugin.getStatTypes().get("Lastjoin"), world)) {
                            has = true;
                        }
                    }
                    if (!has && plugin.getServer().getPlayerExact(old.getPlayername()) == null) {
                        plugin.debug("Temp StatsPlayer object has no Lastjoin value, player is also not online - assuming this player doesn't exist (and therefore, deleting the object)");
                        players.remove(player.getName());
                        return;
                    }
                    players.put(player.getName(), sp);
                    sp.syncData(old);
                } else {
                    players.put(player.getName(), sp);
                }
                if (plugin.getSettings().isInstaUpdateSigns()) {
                    for (StatsSign sign : plugin.getSignManager().getAllSigns()) {
                        if (sign.getSignType().equals(SignType.PLAYER) && sign.getVariable().equals(Integer.toString(sp.getId()))) {
                            sp.addSignReference(sign, sign.getWorld());
                            sign.setAttachedToStat(true);
                        }
                    }
                }
                StatsPlayerLoadedEvent event = new StatsPlayerLoadedEvent(sp, true);
                plugin.getServer().getPluginManager().callEvent(event);
            }
        });
    }

    @Override
    public void unloadPlayer(OfflinePlayer player) {
        this.players.remove(player.getName());
    }

    @Override
    public void unloadPlayer(StatsPlayer player) {
        this.players.remove(player.getPlayername());
    }

}
