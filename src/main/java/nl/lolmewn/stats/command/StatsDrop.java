package nl.lolmewn.stats.command;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.DefaultStat;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Lolmewn
 */
public class StatsDrop extends StatsSubCommand {

    public StatsDrop(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Correct usage: /stats drop <statName>");
            sender.sendMessage("Please note that you cannot drop stats that are provided by default.");
            sender.sendMessage("You can disable those in the config if you wish");
            return true;
        }
        String statName = args[0];
        Stat stat = getPlugin().getAPI().getStat(statName);
        if (stat == null) {
            sender.sendMessage("Stat not found!");
            return true;
        }
        if (stat instanceof DefaultStat) {
            sender.sendMessage("You cannot drop stats that are provided by default.");
            sender.sendMessage("You can disable those in the config, if you wish.");
            return true;
        }
        sender.sendMessage("Dropping stat for all online players...");
        for (StatsPlayer player : getPlugin().getPlayerManager().getPlayers()) {
            player.removeStat(stat);
        }
        getPlugin().getStatTypes().remove(stat.getName());
        this.dropStat(stat.getName());
        sender.sendMessage("Dropping column in database...");
        Connection con = getPlugin().getMySQL().getConnection();
        try {
            stat.getTable().dropColumn(con, stat.getName());
        } catch (SQLException ex) {
            Logger.getLogger(StatsDrop.class.getName()).log(Level.SEVERE, null, ex);
            sender.sendMessage("Something failed while removing the column, please check the database");
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(StatsDrop.class.getName()).log(Level.SEVERE, null, ex);
                sender.sendMessage("Something failed while removing the column, please check the database");
            }
        }
        sender.sendMessage("Stat " + stat.getName() + " dropped succesfully.");
        return true;
    }

    private void dropStat(String statName) {
        File file = new File(getPlugin().getDataFolder(), "stats.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
                return;
            } catch (IOException ex) {
                Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        YamlConfiguration yaml = YamlConfiguration.loadConfiguration(file);
        List<String> stats = yaml.getStringList("stats");
        stats.remove(statName);
        yaml.set("stats", stats);
        try {
            yaml.save(file);
        } catch (IOException ex) {
            Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
