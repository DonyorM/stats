package nl.lolmewn.stats.command;

import nl.lolmewn.stats.Main;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public class StatsSendStats extends StatsSubCommand {

    public StatsSendStats(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        sender.sendMessage(getPlugin().sendToGlobal(getPlugin().server));
        return true;
    }

}
