package nl.lolmewn.stats.command;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsPlayerLoadedEvent;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Lolmewn
 */
public class StatsWorld extends StatsSubCommand implements Listener {

    private final HashMap<String, String> awaitingLoad = new HashMap<String, String>();

    public StatsWorld(Main main, String perm) {
        super(main, perm);
        main.getServer().getPluginManager().registerEvents(this, main);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Correct usage: /stats world <world> [player]");
            return true;
        }
        String worldName = args[0];
        World world = findWorld(worldName);
        if (world == null) {
            sender.sendMessage("This world does not exist!");
            return true;
        }
        if (args.length == 1) {
            //self
            if (!(sender instanceof Player)) {
                sender.sendMessage("This command is for players only");
                return true;
            }
            Player player = (Player) sender;
            this.sendSomeCoolStats(sender, player, world.getName());
        } else {
            if (!sender.hasPermission("stats.view.others")) {
                sender.sendMessage(ChatColor.RED + "Sorry, you do not have permissions to view someone elses stats!");
                return true;
            }
            String playerName = args[1];
            Player player = this.getPlugin().getServer().getPlayer(playerName);
            if (player == null) {
                sender.sendMessage(ChatColor.RED + "Loading player (if he exists...)");
                this.awaitingLoad.put(sender.getName() + ":" + world.getName(), args[0]);
                this.getPlugin().getPlayerManager().loadPlayer(this.getPlugin().getServer().getOfflinePlayer(playerName));
                return true;
            }
            this.sendSomeCoolStats(sender, player, worldName);
        }
        return true;
    }

    private World findWorld(String name) {
        for (World world : this.getPlugin().getServer().getWorlds()) {
            if (world.getName().equalsIgnoreCase(name)) {
                return world;
            }
        }
        for (World world : this.getPlugin().getServer().getWorlds()) {
            if (world.getName().toLowerCase().startsWith(name.toLowerCase())) {
                return world;
            }
        }
        return null;
    }

    @EventHandler
    public void onPlayerLoaded(StatsPlayerLoadedEvent event) {
        if (this.awaitingLoad.containsValue(event.getStatsPlayer().getPlayername())) {
            for (String key : this.awaitingLoad.keySet()) {
                String playerName = key.split(":")[0];
                String world = key.split(":")[1];
                Player player = this.getPlugin().getServer().getPlayerExact(playerName);
                if (player == null) {
                    this.awaitingLoad.remove(key);
                    continue;
                }
                if (this.awaitingLoad.get(key).equalsIgnoreCase(event.getStatsPlayer().getPlayername())) {
                    this.sendSomeCoolStats(player, this.getPlugin().getServer().getOfflinePlayer(event.getStatsPlayer().getPlayername()), world);
                    this.awaitingLoad.remove(key);
                }
            }
        }
    }

    public void sendSomeCoolStats(CommandSender sender, OfflinePlayer from, String world) {
        StatsPlayer lookup = this.getPlugin().getPlayerManager().getPlayer(from);
        int shown = 0;
        for (String type : this.getPlugin().getSettings().getCommand()) {
            Stat stat = this.getPlugin().getStatTypes().get(type);
            if (stat == null) {
                stat = this.getPlugin().getStatTypes().get((type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase()).replace("_", " "));
                if (stat == null) {
                    this.getPlugin().getLogger().warning("Wrongly configured, looking for Stat " + type + ", but it doesn't exist.");
                    continue;
                }
            }
            String prettyName = (type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase()).replace("_", " ");
            if (!lookup.hasStat(stat)) {
                this.getPlugin().debug("Couldn't find StatData for " + type);
                continue;
            }
            StatData statData = lookup.getStatData(stat, world, false);
            if (statData == null) {
                continue;
            }
            if (stat.getName().equals("Move")) {
                double value = 0;
                for (Object[] vars : statData.getAllVariables()) {
                    value += statData.getValue(vars);
                }
                sender.sendMessage(ChatColor.LIGHT_PURPLE + prettyName + ": " + ChatColor.GREEN + value);
            } else if (stat.getName().equals("Lastjoin") || stat.getName().equals("Lastleave")) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS", Locale.ENGLISH);
                sender.sendMessage(ChatColor.LIGHT_PURPLE + prettyName + ": " + ChatColor.GREEN + sdf.format(new Date((long) statData.getValue(new Object[]{}))));
            } else if (stat.getName().equals("Playtime")) {
                int playTimeSeconds = (int) statData.getValue(new Object[]{});
                final String playtime = String.format("%d days, %02d hours, %02d mins, %02d secs",
                        TimeUnit.SECONDS.toDays(playTimeSeconds),
                        TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                        TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                        TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                sender.sendMessage(ChatColor.LIGHT_PURPLE + "Playtime: " + ChatColor.GREEN + playtime);
            } else {
                long v = 0;
                for (Object[] vars : statData.getAllVariables()) {
                    v += statData.getValue(vars);
                }
                sender.sendMessage(ChatColor.LIGHT_PURPLE + prettyName + ": " + ChatColor.GREEN + v);
            }
            shown++;
        }
        if (shown == 0) {
            sender.sendMessage(ChatColor.RED + "No stats to show!");
        }
    }

}
