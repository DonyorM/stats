package nl.lolmewn.stats.compat;

import net.milkbowl.vault.economy.Economy;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 * @author Sybren
 */
public class EconHandler implements Runnable {

    private final Main plugin;
    private Economy econ;

    public EconHandler(Main main) {
        this.plugin = main;
        if (!this.hasEconomy()) {
            return;
        }
        this.plugin.getServer().getScheduler().runTaskTimer(plugin, this, 100L, 100L);
    }

    @Override
    public void run() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            StatsPlayer sPlayer = plugin.getPlayerManager().getPlayer(player);
            for (World world : plugin.getServer().getWorlds()) {
                StatData data = sPlayer.getStatData(plugin.getStatTypes().get("Money"), world.getName(), false);
                double balance;
                if(plugin.supportsUuids()){
                    balance = econ.getBalance(player, world.getName());
                }else{
                    balance = econ.getBalance(player.getName(), world.getName());
                }
                if(data == null && balance != 0){
                    data = sPlayer.getStatData(plugin.getStatTypes().get("Money"), world.getName(), true);
                }
                if(data == null && balance == 0){
                    continue;
                }
                if (data.hasValue(new Object[]{})) {
                    if (data.getValue(new Object[]{}) != balance) {
                        data.setCurrentValue(new Object[]{}, balance);
                        data.forceUpdate(new Object[]{});
                    }
                } else {
                    data.setCurrentValue(new Object[]{}, balance);
                    data.forceUpdate(new Object[]{});
                }
            }
        }
    }

    private boolean hasEconomy() {
        if (this.econ != null) {
            return true;
        }
        RegisteredServiceProvider<Economy> economyProvider = plugin.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            econ = economyProvider.getProvider();
        }
        return (econ != null);
    }

}
