package nl.lolmewn.stats;

import com.earth2me.essentials.Essentials;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.StatUpdateEvent;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.api.mysql.StatsTableManager;
import nl.lolmewn.stats.command.StatsCommand;
import nl.lolmewn.stats.command.StatsCreate;
import nl.lolmewn.stats.compat.EconHandler;
import nl.lolmewn.stats.compat.VotifierListener;
import nl.lolmewn.stats.player.NamePlayerManager;
import nl.lolmewn.stats.player.PlayerManager;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import nl.lolmewn.stats.player.UUIDPlayerManager;
import nl.lolmewn.stats.signs.SignDataGetter;
import nl.lolmewn.stats.signs.SignListener;
import nl.lolmewn.stats.signs.SignManager;
import nl.lolmewn.stats.snapshot.SnapshotManager;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;
import org.mcstats.Metrics.Graph;
import org.mcstats.Metrics.Plotter;

public class Main extends JavaPlugin implements Listener {

    private Settings settings;
    public boolean newConfig;
    public boolean query = false;
    private MySQL mysql;
    private StatsAPI api;
    private SignManager signManager;
    private SignDataGetter signDataGetter;
    private PlayerManager playerManager;
    private SnapshotManager snapshotManager;
    private StatsTableManager tableManager;
    public String server = "stats.lolmewn.nl"; //default
    private int unableToConnectToGlobal = 0;
    protected boolean beingConfigged = false;
    protected Queue<GlobalDataHolder> globalQueue = new ConcurrentLinkedQueue<GlobalDataHolder>();
    private int queriesExecuted = 0;
    protected double newVersion;
    private StatTypes statTypes;
    public final HashMap<String, String> awaitingPlayerLoad = new HashMap<String, String>();

    private boolean hasEssentials;
    private Essentials ess;

    @Override
    public void onDisable() {
        if (this.signManager != null) {
            this.signManager.save();
        }
        if (this.mysql != null && !this.mysql.isFault()) {
            this.runTableUpdates();
            this.mysql.exit();
        }
        if (this.canSendToGlobal() && this.unableToConnectToGlobal == 0) {
            this.sendToGlobal(server);
        }
        this.getServer().getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        this.settings = new Settings(this);
        this.newConfig = this.setupConfig();
        this.checkSettings();
        this.settings.loadSettings();
        this.tableManager = new StatsTableManager();
        if (this.getServer().getPluginManager().getPlugin("Votifier") != null) {
            this.getServer().getPluginManager().registerEvents(new VotifierListener(this), this);
        }
        if (this.getServer().getPluginManager().getPlugin("Essentials") != null) {
            this.hasEssentials = true;
            this.ess = (Essentials) this.getServer().getPluginManager().getPlugin("Essentials");
        }
        if (this.getServer().getPluginManager().getPlugin("Vault") != null && !this.settings.getDisabledStats().contains("Money")) {
            new EconHandler(this);
        }
        if (!newConfig) {
            this.mysql = new MySQL(this, this.getSettings().getDbHost(), this.getSettings().getDbPort(),
                    this.getSettings().getDbUser(), this.getSettings().getDbPass(),
                    this.getSettings().getDbName(), this.getSettings().getDbPrefix());
            if (this.mysql.isFault()) {
                this.getLogger().severe("MySQL connection failed, disabling plugin!");
                this.getServer().getPluginManager().disablePlugin(this);
                return;
            }
            if (this.mysql.usesOldFormat()) {
                this.mysql.convertOldFormat();
            }
        }
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        //Schedule Play time rec every second.
        if (!this.getSettings().getDisabledStats().contains("Playtime")) {
            schedulePlaytimeRecording();
        }
        //Schedule the updating of all tables and some debug
        scheduleTableUpdates();
        //Enable (or not) Sending of data to global server
        if (this.getSettings().isSendToGlobal()) {
            if (this.canSendToGlobal()) {
                this.debug("Setting up globalserver sending every 600 ticks.");
                scheduleGlobalStatsSending();
            } else {
                this.getLogger().warning("Not sending to global server due to online-mode=false");
                this.getLogger().warning("To fully use all of this plugins capabilities, please use online-mode=true");
            }
        }
        if (this.newConfig) {
            this.getServer().getPluginManager().registerEvents(new Listener() {
                @EventHandler
                public void join(PlayerJoinEvent e) {
                    if (!newConfig) {
                        return;
                    }
                    if (e.getPlayer().hasPermission("stats.config") || e.getPlayer().isOp() && !beingConfigged) {
                        beingConfigged = true;
                        startConfigurator(e.getPlayer());
                    }
                }
            }, this);
        }
        startMetrics();
        registerAPI();
        this.statTypes = new StatTypes();
        new DefaultStatsAndTables().loadDefaults(this, api);
        loadCustomStats();
        this.signManager = new SignManager(this);

        if (!newConfig) {
            this.getServer().getScheduler().runTask(this, new Runnable() {
                @Override
                public void run() {
                    signManager.load();
                    Connection con = getMySQL().getConnection();
                    for (StatsTable table : getStatsTableManager().values()) {
                        String create = null;
                        try {
                            Statement st = con.createStatement();
                            create = table.generateCreateTable();
                            st.executeUpdate(create);
                            table.validateColumns(con);
                            st.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                            getLogger().warning("The above warning was probably thrown by this statement: " + create);
                        }
                    }
                    StatsTable infoTable = getStatsTableManager().get(getSettings().getDbPrefix() + "system");
                    try {
                        infoTable.getColumn("version").dbSet(con, null, null, getDescription().getVersion());
                        infoTable.getColumn("timezone").dbSet(con, null, null, TimeZone.getDefault().getID());
                        con.close();
                    } catch (SQLException e) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                    }
                }
            });
        }
        this.signDataGetter = new SignDataGetter(this);
        if (this.supportsUuids()) {
            this.playerManager = new UUIDPlayerManager(this);
            this.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "Using UUID for Player management");
        } else {
            this.playerManager = new NamePlayerManager(this);
            this.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "Using player names for Player management as UUIDs were not found");
        }
        SignListener signListener = new SignListener(this);
        this.getServer().getPluginManager().registerEvents(signListener, this);
        if (!this.getSettings().isInstaUpdateSigns()) {
            StatUpdateEvent.getHandlerList().unregister(signListener);
        }
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, signDataGetter, getSettings().getSignsUpdateInterval() * 20, getSettings().getSignsUpdateInterval() * 20);
        scheduleUpdater();
        if (getConfig().contains("lastJoin-lastLeaveFix")) {
            getConfig().set("lastJoin-lastLeaveFix", null);
            saveConfig();
        }
        if (this.getSettings().createSnapshots()) {
            this.snapshotManager = new SnapshotManager(this,
                    this.getTimeUntilNextSnapshot() / 50,
                    this.convertIntervalToLong(this.getSettings().getSnapshotInterval()) / 50,
                    this.convertIntervalToLong(this.getSettings().getSnapshotTTL()));
        }
        this.getCommand("lolmewnstats").setExecutor(new StatsCommand(this));
        this.getLogger().info("Version " + this.getDescription().getVersion() + " enabled!");
    }

    protected String getBlockTable() {
        return this.getSettings().getDbPrefix() + "block";
    }

    protected String getMoveTable() {
        return this.getSettings().getDbPrefix() + "move";
    }

    protected String getKillTable() {
        return this.getSettings().getDbPrefix() + "kill";
    }

    protected String getDeathTable() {
        return this.getSettings().getDbPrefix() + "death";
    }

    protected String getPlayerTable() {
        return this.getSettings().getDbPrefix() + "player";
    }

    public StatsTableManager getStatsTableManager() {
        return this.tableManager;
    }

    public SignManager getSignManager() {
        return this.signManager;
    }

    public Settings getSettings() {
        return this.settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public PlayerManager getPlayerManager() {
        return this.playerManager;
    }

    public SnapshotManager getSnapshotManager() {
        return snapshotManager;
    }

    public MySQL getMySQL() {
        return this.mysql;
    }

    public void setMysql(MySQL mysql) {
        this.mysql = mysql;
    }

    private boolean setupConfig() {
        if (!new File(this.getDataFolder(), "config.yml").exists()) {
            this.saveResource("config.yml", false);
            return true;
        }
        return false;
    }

    private void checkSettings() {
        getConfig().options().copyDefaults(true);
        getConfig().set("snapshots.cross-server", null);
        if (getConfig().contains("useBetaFunctions")) {
            if (!getConfig().getBoolean("useBetaFunctions") && getConfig().getBoolean("snapshots.enabled")) {
                getConfig().set("useBetaFunctions", null);
                getConfig().set("snapshots.enabled", false);
            }
        }
        saveConfig();
    }

    public void debug(String message) {
        if (this.getSettings().isDebugging()) {
            this.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "[Debug] " + message));
        }
    }

    public void debugQuery(String message) {
        if (this.query) {
            this.getLogger().info("[Debug][Q] " + message);
        }
    }// This method returns whether or not the server supports UUIDs

    public boolean supportsUuids() {
        String packageName = this.getServer().getClass().getPackage().getName();
        // Get full package string of CraftServer.
        // org.bukkit.craftbukkit.versionstring (or for pre-refactor, just org.bukkit.craftbukkit
        String version = packageName.substring(packageName.lastIndexOf('.') + 1);
        if (version.equals("craftbukkit")) {
            return false;
        }
        String[] split = version.split("_");
        if (split.length != 3) {
            getLogger().log(Level.SEVERE, "Package version specifier of unknown format found: '" + version + "' - this will prevent Stats from confirming server version.");
            getLogger().log(Level.SEVERE, "If you are running minecraft version 1.7.3 or higher and still get this message, please contact Lolmewn.");
            return false;
        }
        int first, second, third;
        try {
            first = Integer.parseInt(split[0].substring(1)); // substring for v1 -> 1
            second = Integer.parseInt(split[1]);
            third = Integer.parseInt(split[2].substring(1)); // substring for R1 -> 1
        } catch (NumberFormatException ignored) {
            getLogger().log(Level.SEVERE, "Package version specifier of unknown format found: '" + version + "' - this will prevent Stats from confirming server version.");
            getLogger().log(Level.SEVERE, "If you are running minecraft version 1.7.3 or higher and still get this message, please contact Lolmewn.");
            return false;
        }
        // if we're on minecraft v2.X, the other version parts don't matter
        return first > 1 || second > 7 || (second == 7 && third >= 3);
    }

    private boolean sendingStatsGlobal = false;

    public String sendToGlobal(String server) {
        if (this.sendingStatsGlobal) {
            return "Stats already sending, cancelling.";
        }
        Socket soc = null;
        try {
            this.debug("Connecting to global server...");
            soc = new Socket(server, 1888);
            this.sendingStatsGlobal = true;
            this.debug("Connected to global server.");
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            out.println("GSPV1");
            if (!in.readLine().equalsIgnoreCase("ready for servername")) {
                debug("Global server didn't want servername :'(");
            }
            out.println(this.getServer().getServerName());
            if (!in.readLine().equalsIgnoreCase("ready for serverport")) {
                debug("Global server didn't want serverport :'(");
            }
            out.println(this.getServer().getPort());
            if (this.globalQueue.isEmpty()) {
                out.println("ping");
                in.close();
                out.flush();
                out.close();
                soc.close();
                this.sendingStatsGlobal = false;
                return "Just pinged.";
            } else {
                while (!this.globalQueue.isEmpty()) {
                    GlobalDataHolder h = this.globalQueue.poll();
                    out.println(h.getPlayer());
                    out.println(h.getStat());
                    out.println(h.getTable());
                    out.println(h.getParameters().length);
                    for (Object o : h.getParameters()) {
                        out.println(o);
                    }
                    out.println(h.getUpdateValue());
                    out.println(h.isIncrementing());
                }
                out.println("end");
                String read = in.readLine();
                this.unableToConnectToGlobal = 0;
                in.close();
                out.flush();
                out.close();
                this.sendingStatsGlobal = false;
                return read;
            }

        } catch (UnknownHostException ex) {
            this.getLogger().warning("Couldn't connect to global server! Your DNS lookup might be broken or inactive!");
        } catch (ConnectException ex) {
            if (this.unableToConnectToGlobal == 0 || this.unableToConnectToGlobal % 60 == 0) {
                this.getLogger().warning("Couldn't connect to global server! Maybe it's offline...");
            }
            this.unableToConnectToGlobal++;
        } catch (IOException ex) {
            if (this.unableToConnectToGlobal == 0 || this.unableToConnectToGlobal % 60 == 0) {
                this.getLogger().warning("Couldn't connect to global server! Maybe it's offline...");
            }
            this.unableToConnectToGlobal++;
        } catch (NullPointerException ex) {
            if (this.unableToConnectToGlobal == 0 || this.unableToConnectToGlobal % 60 == 0) {
                this.getLogger().warning("Couldn't connect to global server! Maybe it's offline...");
            }
            this.unableToConnectToGlobal++;
        } finally {
            this.sendingStatsGlobal = false;
            try {
                if (soc != null) {
                    soc.close();
                }
            } catch (IOException ex) {
            }
        }
        return "Something failed while trying to send stats to global server!";
    }
    private boolean working;

    public void runTableUpdates() {
        if (working) {
            debug("stats were still sending, postphoning.");
            return;
        }
        working = true;
        long start = System.nanoTime();
        Connection con = getMySQL().getConnection();
        try {
            this.updateStatsInDatabase(con);
        } catch (Exception e) {
            System.out.println("Exception happened: " + e.getMessage());
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
            working = false;
        }
        working = false;
        if (!this.canSendToGlobal()) {
            this.globalQueue.clear();
        }
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        double taken = (System.nanoTime() - start) / 1000000;
        if (taken != 0) {
            this.debug("Time taken sending table updates: " + taken);
        }
    }

    private void updateStatsInDatabase(Connection con) {
        boolean canSendToGlobal = this.canSendToGlobal();
        try {
            con.setAutoCommit(false);
            for (StatsPlayer player : this.getPlayerManager().getPlayers()) {
                if (player.isTemp()) {
                    continue;
                }
                if (canSendToGlobal) {
                    queueToGlobal(player);
                }
                for (StatsTable table : this.getStatsTableManager().values()) {
                    for (Stat stat : table.getAssociatedStats()) {
                        table.updateStat(con, stat, player);
                    }
                }
                if (this.getServer().getPlayerExact(player.getPlayername()) == null) {
                    this.getPlayerManager().unloadPlayer(player);
                }
            }
            con.commit();
        } catch (SQLException e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    protected boolean setupMySQL(ConversationContext cc) {
        this.mysql = new MySQL(this,
                (String) cc.getSessionData("MySQL-Host"),
                Integer.parseInt((String) cc.getSessionData("MySQL-Port")),
                (String) cc.getSessionData("MySQL-User"),
                (String) cc.getSessionData("MySQL-Pass"),
                (String) cc.getSessionData("MySQL-Database"),
                "Stats_");
        return !this.mysql.isFault();
    }

    protected void startConfigurator(Player p) {
        new Configurator(this, p);
    }

    protected void saveValues(ConversationContext cc) {
        getConfig().set("MySQL-Host", (String) cc.getSessionData("MySQL-Host"));
        getConfig().set("MySQL-Port", cc.getSessionData("MySQL-Port"));
        getConfig().set("MySQL-User", (String) cc.getSessionData("MySQL-User"));
        getConfig().set("MySQL-Pass", (String) cc.getSessionData("MySQL-Pass"));
        getConfig().set("MySQL-Database", (String) cc.getSessionData("MySQL-Database"));
        saveConfig();
    }

    private void registerAPI() {
        this.api = new StatsAPI(this);
        this.getServer().getServicesManager().register(StatsAPI.class, api, this, ServicePriority.Low);
    }

    public StatsAPI getAPI() {
        return api;
    }

    protected boolean canSendToGlobal() {
        return this.getSettings().isSendToGlobal() && (this.getServer().getOnlineMode() || this.getServer().getIp().equals("127.0.0.1") || this.getServer().getIp().equals("localhost"));
    }

    public void configComplete() {
        signManager.load();
        Connection con = getMySQL().getConnection();
        for (StatsTable table : getStatsTableManager().values()) {
            try {
                Statement st = con.createStatement();
                String create = table.generateCreateTable();
                st.executeUpdate(create);
                table.validateColumns(con);
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            con.close();
        } catch (SQLException e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        }
        for (Player p : this.getServer().getOnlinePlayers()) {
            this.getPlayerManager().addPlayer(p, new StatsPlayer(this, p.getName(), true));
            this.getPlayerManager().loadPlayer(p);
        }
    }

    /**
     * Is only being ran if config value is true
     */
    private void scheduleUpdater() {
        final Main plugin = this;
        if (!this.getSettings().isUpdating()) {
            return;
        }
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                new Updater(plugin, 43564, plugin.getFile(), Updater.UpdateType.DEFAULT, false);
            }
        }, 0, 72000);
    }

    private void schedulePlaytimeRecording() {
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for (Player p : getServer().getOnlinePlayers()) {
                    if ((getSettings().isUsingPermissionsForTracking() && !p.hasPermission("stats.track") && !p.hasPermission("stats.track.playtime")) || p.hasMetadata("NPC")) {
                        continue;
                    }
                    if (getSettings().isIgnoreAFK() && hasEssentials && ess.getUser(p).isAfk()) {
                        continue;
                    }
                    getPlayerManager().getPlayer(p).getStatData(getStatTypes().get("Playtime"), p.getLocation().getWorld().getName(), true).addUpdate(new Object[]{}, 1);
                }
            }
        }, 20L, 20L);
    }

    private void scheduleTableUpdates() {
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                if (!newConfig) {
                    runTableUpdates();
                }
            }
        }, 200L, 200L);
    }

    private void scheduleGlobalStatsSending() {
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                String re = sendToGlobal(server);
                if (!re.equals("")) {
                    debug(re);
                }
            }
        }, 700, 600);
    }

    private void startMetrics() {
        try {
            Metrics m = new Metrics(this);
            Graph g = m.createGraph("Sending Data to Global Server");
            g.addPlotter(new Plotter() {
                @Override
                public String getColumnName() {
                    return "Enabled in config";
                }

                @Override
                public int getValue() {
                    return getSettings().isSendToGlobal() ? 1 : 0;
                }
            });
            g.addPlotter(new Plotter() {
                @Override
                public String getColumnName() {
                    return "Allowed to send";
                }

                @Override
                public int getValue() {
                    return canSendToGlobal() ? 1 : 0;
                }
            });
            Graph d = m.createGraph("Queries executed");
            d.addPlotter(new Plotter() {
                @Override
                public int getValue() {
                    if (mysql == null) {
                        return 0;
                    }
                    int done = queriesExecuted;
                    queriesExecuted = 0;
                    return done;
                }
            });
            m.addGraph(g);
            m.addGraph(d);
            m.start();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private long getTimeUntilNextSnapshot() {
        if (System.currentTimeMillis() > this.getSettings().getPreviousSnapshot() + this.convertIntervalToLong(this.getSettings().getSnapshotInterval())) {
            return 0L;
        }
        return this.convertIntervalToLong(this.getSettings().getSnapshotInterval()) - (System.currentTimeMillis() - this.getSettings().getPreviousSnapshot());
    }

    private long convertIntervalToLong(String interval) {
        String[] split = interval.split(",");
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(0);
        for (String str : split) {
            int type;
            switch (str.charAt(str.length() - 1)) {
                case 'm':
                    type = Calendar.MINUTE;
                    break;
                case 'h':
                case 'H':
                    type = Calendar.HOUR;
                    break;
                case 'd':
                case 'D':
                    type = Calendar.DATE;
                    break;
                case 'w':
                case 'W':
                    cal.add(Calendar.DATE, 7 * Integer.valueOf(str.substring(0, str.length() - 1)));
                    continue;
                case 'M':
                    type = Calendar.MONTH;
                    break;
                case 'y':
                case 'Y':
                    type = Calendar.YEAR;
                    break;
                default:
                    continue;
            }
            cal.add(type, Integer.valueOf(str.substring(0, str.length() - 1)));
        }
        return cal.getTimeInMillis();
    }

    public StatTypes getStatTypes() {
        return this.statTypes;
    }

    private void queueToGlobal(StatsPlayer player) {
        for (String world : player.getWorlds()) {
            for (StatData data : player.getStatsForWorld(world)) {
                if (data.getUpdateVariables().isEmpty()) {
                    continue;
                }
                if (!(data.getStat() instanceof DefaultStat)) {
                    continue;
                }
                for (Object[] updates : data.getUpdateVariables()) {
                    this.globalQueue.add(new GlobalDataHolder(player.getPlayername(),
                            data.getStat().getName().replace(" ", ""),
                            data.getStat().getTable().getName().replaceFirst(this.getSettings().getDbPrefix(), ""),
                            updates,
                            data.getStat().getDataType().equals(StatDataType.INCREASING) ? data.getUpdateValue(updates, false) : data.getValue(updates),
                            data.getStat().getDataType().equals(StatDataType.INCREASING)));
                }
            }
        }
    }

    private void loadCustomStats() {
        File file = new File(this.getDataFolder(), "stats.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
                return;
            } catch (IOException ex) {
                Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        YamlConfiguration yaml = YamlConfiguration.loadConfiguration(file);
        List<String> stats = yaml.getStringList("stats");
        StatsTable playerTable = this.getStatsTableManager().get(this.getSettings().getDbPrefix() + "player");
        for (String statName : stats) {
            Stat stat = getAPI().addStat(
                    statName,
                    StatDataType.DYNAMIC,
                    StatTableType.COLUMN,
                    playerTable,
                    MySQLType.INTEGER,
                    statName,
                    null);
            playerTable.addStat(stat, "0");
        }
    }
}
