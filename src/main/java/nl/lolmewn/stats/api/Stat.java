package nl.lolmewn.stats.api;

import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.api.saver.DataSaver;

/**
 * @author Lolmewn
 */
public class Stat {

    private final String name;
    private final StatDataType type;

    private final transient StatTableType tableType;
    private final StatsTable table;
    private final transient MySQLType mysqlType;

    private transient DataLoader dataLoader;

    private transient DataSaver dataSaver;

    private String valueColumn;
    private String[] varColumns;

    public Stat(String name, StatDataType dataType, StatTableType tableType, StatsTable table, MySQLType type, DataLoader dataLoader, DataSaver dataSaver) {
        this.name = name;
        this.table = table;
        this.tableType = tableType;
        this.type = dataType;
        this.mysqlType = type;
        this.dataLoader = dataLoader;
        this.dataSaver = dataSaver;
    }

    public Stat(String name, StatDataType dataType, StatTableType tableType, StatsTable table, MySQLType type, String valueColumn, String[] varColumns) {
        this.name = name;
        this.table = table;
        this.tableType = tableType;
        this.type = dataType;
        this.mysqlType = type;
        this.valueColumn = valueColumn;
        if (varColumns == null) {
            this.varColumns = new String[0];
        } else {
            this.varColumns = varColumns;
        }
    }

    public String getName() {
        return name;
    }

    public String getValueColumn() {
        return valueColumn;
    }

    public String[] getVarColumns() {
        return varColumns;
    }

    public DataLoader getDataLoader() {
        return dataLoader;
    }

    public DataSaver getDataSaver() {
        return dataSaver;
    }

    public StatDataType getDataType() {
        return type;
    }

    public StatTableType getTableType() {
        return tableType;
    }

    public StatsTable getTable() {
        return table;
    }

    public MySQLType getMySQLType() {
        return this.mysqlType;
    }

}
