package nl.lolmewn.stats.api.saver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class DefaultDataSaver extends DataSaver {

    public DefaultDataSaver(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean save(StatsPlayer player, Stat stat, Connection con) throws SQLException {
        for (String world : player.getWorlds()) {
            StatData data = player.getStatData(stat, world, false);
            if (data == null) {
                continue;
            }
            List<Object[]> updates = new ArrayList<Object[]>(data.getUpdateVariables());
            if (updates.isEmpty()) {
                continue;
            }
            if (updates.size() != 1) {
                System.out.println("[Stats] Don't know how to save "
                        + stat.getName()
                        + " for player "
                        + player.getPlayername()
                        + " in world "
                        + world
                        + ", there's more than one update!");
                continue;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ").append(stat.getTable().getName()).append(" SET ");
            sb.append(stat.getName().toLowerCase().replace(" ", ""));
            sb.append("=");
            switch (stat.getDataType()) {
                case INCREASING:
                    sb.append("IFNULL(").append(stat.getName().toLowerCase().replace(" ", "")).append(", 0)").append("+");
                default:
                    sb.append("?");
            }
            sb.append(" WHERE player_id=? AND world=? ");
            if (this.getAPI().isCreatingSnapshots()) {
                sb.append("AND snapshot_name=?");
            }
            PreparedStatement st = con.prepareStatement(sb.toString());
            double value = stat.getDataType().equals(StatDataType.INCREASING) ? data.getUpdateValue(updates.get(0), true) : data.getValue(updates.get(0), true);
            if (stat.getMySQLType().equals(MySQLType.TIMESTAMP)) {
                st.setTimestamp(1, new Timestamp((long) value));
            } else {
                st.setDouble(1, value);
            }
            st.setInt(updates.size() + 1, player.getId());
            st.setString(updates.size() + 2, world);
            if (this.getAPI().isCreatingSnapshots()) {
                st.setString(updates.size() + 3, "main_snapshot");
            }
            int updatedRows = st.executeUpdate();
            st.close();
            if (updatedRows == 0) {
                sb = new StringBuilder();
                sb.append("INSERT INTO ").append(stat.getTable().getName()).append(" (player_id, world, ");
                if (this.getAPI().isCreatingSnapshots()) {
                    sb.append("snapshot_name, ");
                }
                sb.append(stat.getName().toLowerCase().replace(" ", "")).append(") VALUES (?,?,?");
                if (this.getAPI().isCreatingSnapshots()) {
                    sb.append(",?");
                }
                sb.append(")");
                st = con.prepareStatement(sb.toString());
                st.setInt(1, player.getId());
                st.setString(2, world);
                if (this.getAPI().isCreatingSnapshots()) {
                    st.setString(3, "main_snapshot");
                }
                if (stat.getMySQLType().equals(MySQLType.TIMESTAMP)) {
                    st.setTimestamp(this.getAPI().isCreatingSnapshots() ? 4 : 3, new Timestamp((long) value));
                } else {
                    st.setDouble(this.getAPI().isCreatingSnapshots() ? 4 : 3, value);
                }
                st.executeUpdate();
                st.close();
            }
        }
        return true;
    }
}
