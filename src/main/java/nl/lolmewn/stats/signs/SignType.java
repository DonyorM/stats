/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public enum SignType {

    PLAYER,
    GLOBAL,
    CUSTOM,
    RIGHTCLICK;

    public static SignType fromString(String input) {
        if (input != null) {
            for (SignType t : SignType.values()) {
                if (t.toString().equalsIgnoreCase(input.toUpperCase())) {
                    return t;
                }
            }
        }
        throw new IllegalArgumentException("No constant with text " + input + " found");
    }

}
