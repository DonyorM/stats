package nl.lolmewn.stats.snapshot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.mysql.MySQLAttribute;
import nl.lolmewn.stats.api.mysql.StatsColumn;
import nl.lolmewn.stats.api.mysql.StatsTable;

/**
 * @author Sybren
 */
public class SnapshotManager {

    private final Main plugin;
    private long timeUntilNextSnapshot, interval, timeToLive;
    private boolean sending;

    public SnapshotManager(Main m, long timeUntilNextSnapshot, long interval, long timeToLive) {
        this.plugin = m;
        this.timeUntilNextSnapshot = timeUntilNextSnapshot;
        this.interval = interval;
        this.timeToLive = timeToLive;
        this.startRunnables();
    }

    public final void startRunnables() {
        plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                runSnapshotCreator("auto_generated_snapshot");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SnapshotManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                runSnapshotDeletor();
            }
        }, this.timeUntilNextSnapshot < 200 ? 200 : this.timeUntilNextSnapshot, this.interval);
        plugin.getLogger().info("Next snapshot scheduled to run in " + timeUntilNextSnapshot + " ticks");

    }

    private void runSnapshotCreator(String name) {
        try {
            if (sending) {
                plugin.getLogger().warning("Snapshot creator was still running, aborting!");
                plugin.getConfig().set("snapshots.previous", System.currentTimeMillis());
                plugin.saveConfig();
                return;
            }
            sending = true;
            long timeStarted = System.nanoTime();
            Connection con = plugin.getMySQL().getConnection();
            for (StatsTable table : plugin.getStatsTableManager().values()) {
                if(!table.hasColumn("snapshot_time") || !table.hasColumn("snapshot_name")){
                    continue; // This table doesn't use snapshots
                }
                StringBuilder sb = new StringBuilder();
                sb.append("INSERT INTO ").append(table.getName()).append("(");
                Collection<String> columns = table.getColumnNames();
                Iterator<String> it = columns.iterator();
                boolean timeFirst = false;
                boolean nameFirst = false;
                StringBuilder columnBuilder = new StringBuilder();
                while (it.hasNext()) {
                    String column = it.next();
                    StatsColumn sCol = table.getColumn(column);
                    if (sCol.hasAttributes() && sCol.getAttributes().contains(MySQLAttribute.PRIMARY_KEY)) {
                        continue;//ignore primary key
                    }
                    if (column.equals("snapshot_time") && !nameFirst && !timeFirst) {
                        timeFirst = true;
                    } else if (column.equals("snapshot_name")) {
                        nameFirst = true;
                    }
                    columnBuilder.append(column);
                    if (it.hasNext()) {
                        columnBuilder.append(", ");
                    }
                }
                sb.append(columnBuilder.toString());
                sb.append(") SELECT ").append(columnBuilder.toString().replace("snapshot_name", "?").replace("snapshot_time", "?"));
                sb.append(" FROM ").append(table.getName()).append(" WHERE snapshot_name=?");
                PreparedStatement st = con.prepareStatement(sb.toString());
                if (nameFirst) {
                    st.setString(1, name);
                    st.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                    st.setString(3, "main_snapshot");
                } else {
                    st.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                    st.setString(2, name);
                    st.setString(3, "main_snapshot");
                }
                st.executeUpdate();
                st.close();
            }
            long timeTaken = (System.nanoTime() - timeStarted) / 1000000;
            plugin.getLogger().info("Snapshot created succesfully in " + timeTaken + "ms");
            plugin.getConfig().set("snapshots.previous", System.currentTimeMillis());
            plugin.saveConfig();
            con.close();
            sending = false;
        } catch (SQLException ex) {
            plugin.getLogger().severe("Couldn't create Snapshot;");
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            sending = false;
        }
    }

    private void runSnapshotDeletor() {
        try {
            Connection con = plugin.getMySQL().getConnection();
            for (StatsTable table : plugin.getStatsTableManager().values()) {
                if(!table.hasColumn("snapshot_time") || !table.hasColumn("snapshot_name")){
                    continue; // This table doesn't use snapshots
                }
                String query = "DELETE FROM " + table.getName() + " "
                        + "WHERE snapshot_time < (NOW() - " + timeToLive + "/1000)"
                        + " AND snapshot_name = 'auto_generated_snapshot'";
                Statement st = con.createStatement();
                st.execute(query);
                st.close();
            }
            con.close();
            plugin.getLogger().info("Old snapshots deleted.");
        } catch (SQLException ex) {
            plugin.getLogger().severe("Couldn't delete Snapshots;");
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public void setTimeUntilNextSnapshot(long timeUntilNextSnapshot) {
        this.timeUntilNextSnapshot = timeUntilNextSnapshot;
    }
}
