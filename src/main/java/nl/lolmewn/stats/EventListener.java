package nl.lolmewn.stats;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class EventListener implements Listener {

    private final Main plugin;
    private final String permissionNode = "stats.track";
    private ConcurrentHashMap<String, Double> moveHolder = new ConcurrentHashMap<String, Double>();
    private final Object[] empty = new Object[]{};

    private Main getPlugin() {
        return this.plugin;
    }

    public EventListener(Main aThis) {
        this.plugin = aThis;
        this.plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                handleMovers();
            }
        }, 20L, 20L);
    }

    private void handleMovers() {
        for (String key : moveHolder.keySet()) {
            if (moveHolder.get(key) == 0) {
                continue;
            }
            String[] args = key.split(",");
            int type = Integer.parseInt(args[0].substring(1));
            String player = args[1].substring(1);
            String world = args[2].substring(1, args[2].indexOf("]"));
            plugin.getAPI().getPlayer(this.plugin.getServer().getOfflinePlayer(player)).getStatData(getStat("Move"), world, true).addUpdate(new Object[]{type}, moveHolder.get(key));
            moveHolder.replace(key, 0d);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".blockbreak")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Block break")) {
            return;
        }
        if (event.getBlock().getType().equals(Material.AIR)) {
            return;
        }
        plugin.getAPI().getPlayer(event.getPlayer()).getStatData(getStat("Block break"), event.getBlock().getWorld().getName(), true).addUpdate(new Object[]{event.getBlock().getTypeId(), event.getBlock().getData(), true}, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".blockplace")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Block place")) {
            return;
        }
        plugin.getAPI().getPlayer(event.getPlayer()).getStatData(getStat("Block place"), event.getBlock().getWorld().getName(), true).addUpdate(new Object[]{event.getBlock().getTypeId(), event.getBlock().getData(), false}, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(final PlayerMoveEvent event) {
        if (event instanceof PlayerTeleportEvent) {
            return;
        }
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".move")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreFlying() && event.getPlayer().isFlying()) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Move")) {
            return;
        }
        if (!event.getFrom().getWorld().equals(event.getTo().getWorld())) {
            return;
        }
        Player p = event.getPlayer();
        double distance = event.getFrom().distance(event.getTo());
        if (distance < 0.0001 || distance > 10) {
            return;
        }
        int type = 0;
        if (p.isInsideVehicle()) {
            Entity vehicle = p.getVehicle();
            if (vehicle instanceof Boat) {
                type = 1;
            } else if (vehicle instanceof Minecart) {
                type = 2;
            } else if (vehicle instanceof Pig) {
                if (vehicle.isInsideVehicle() && vehicle.getVehicle() instanceof Minecart) {
                    type = 4;
                } else {
                    type = 3;
                }
            } else {
                try {
                    if (vehicle instanceof Horse) {
                        type = 5;
                    }
                } catch (Exception e) {
                    //MC version doesn't have horses yet :O
                }

            }
        }
        String key = Arrays.toString(new Object[]{type, event.getPlayer().getName(), event.getFrom().getWorld().getName()});
        if (this.moveHolder.replace(key, distance + (this.moveHolder.containsKey(key) ? this.moveHolder.get(key) : 0)) == null) {
            this.moveHolder.put(key, distance);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDeath(EntityDeathEvent event) {
        if (event.getEntity().hasMetadata("NPC")) {
            return;
        }
        EntityType e = event.getEntityType();
        if (event.getEntity().getKiller() != null) {
            if (plugin.getSettings().isUsingPermissionsForTracking() && !event.getEntity().getKiller().hasPermission(permissionNode) && !event.getEntity().getKiller().hasPermission(permissionNode + ".kill")) {
                return;
            }
            if (this.getPlugin().getSettings().isIgnoreCreative() && event.getEntity().getKiller().getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            if (this.getPlugin().getSettings().getDisabledStats().contains("Kill")) {
                return;
            }
            StatsPlayer p = plugin.getAPI().getPlayer(event.getEntity().getKiller());
            p.updateStat(getStat("Kill"), new Object[]{e.toString().substring(0, 1) + e.toString().substring(1).toLowerCase()}, 1, event.getEntity().getWorld().getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerDeathEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getEntity().hasPermission(permissionNode) && !event.getEntity().hasPermission(permissionNode + ".death")) || event.getEntity().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getEntity().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Death")) {
            return;
        }
        EntityDamageEvent e = event.getEntity().getLastDamageCause();
        Player dead = event.getEntity();
        StatsPlayer player = this.getPlugin().getAPI().getPlayer(dead);
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent ev = (EntityDamageByEntityEvent) e;
            if (ev.getDamager() instanceof Arrow) {
                Arrow a = (Arrow) ev.getDamager();
                if (a.getShooter() instanceof LivingEntity) {
                    LivingEntity le = (LivingEntity) a.getShooter();
                    player.updateStat(getStat("Death"), new Object[]{
                        le.getType().toString().substring(0, 1)
                        + le.toString().substring(1).toLowerCase(), true}, 1,
                            dead.getWorld().getName());
                }

            } else {
                player.updateStat(getStat("Death"), new Object[]{ev.getDamager().getType().toString().substring(0, 1)
                    + ev.getDamager().getType().toString().substring(1).toLowerCase(), true}, 1, dead.getWorld().getName());
            }
        } else {
            if (e == null) {
                player.updateStat(getStat("Death"), new Object[]{"Suicide", true}, 1, dead.getWorld().getName());
            } else {
                player.updateStat(getStat("Death"), new Object[]{e.getCause().toString().substring(0, 1)
                    + e.getCause().toString().substring(1).toLowerCase(), true}, 1, dead.getWorld().getName());
            }
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void logout(PlayerQuitEvent event) {
        if (this.getPlugin().getSettings().getDisabledStats().contains("Lastleave") || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        StatsPlayer player = this.getPlugin().getAPI().getPlayer(event.getPlayer());
        StatData stat = player.getStatData(getStat("Lastleave"), event.getPlayer().getWorld().getName(), true);
        stat.setCurrentValue(empty, System.currentTimeMillis());
        stat.forceUpdate(empty);

        this.handleMovers(); //handle him before he gets processed by data saver
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void login(PlayerJoinEvent event) {
        if (event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        StatsPlayer player = this.getPlugin().getAPI().getPlayer(event.getPlayer());
        if (!this.getPlugin().getSettings().getDisabledStats().contains("Lastjoin")) {
            StatData lastjoinStat = player.getStatData(getStat("Lastjoin"), event.getPlayer().getWorld().getName(), true);
            lastjoinStat.setCurrentValue(empty, System.currentTimeMillis());
            lastjoinStat.forceUpdate(empty);
        }
        if (!this.getPlugin().getSettings().getDisabledStats().contains("Joins")) {
            StatData joinsStat = player.getStatData(getStat("Joins"), event.getPlayer().getWorld().getName(), true);
            joinsStat.addUpdate(empty, 1);
        }
        if (!this.getPlugin().getSettings().getDisabledStats().contains("Firstjoin")) {
            StatData firstJoin = player.getStatData(getStat("Firstjoin"), "__GLOBAL__", true);
            firstJoin.setCurrentValue(empty, event.getPlayer().getFirstPlayed());
            firstJoin.forceUpdate(empty);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void XPChange(PlayerExpChangeEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".xpgained")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Xp gained")) {
            return;
        }
        if (event.getAmount() > 0) {
            this.getPlugin().getAPI().getPlayer(event.getPlayer())
                    .getStatData(getStat("Xp gained"), event.getPlayer().getWorld().getName(), true).addUpdate(empty, event.getAmount());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void fish(PlayerFishEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".fishcatched")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Fish catched")) {
            return;
        }
        if (event.getCaught() != null) {
            this.getPlugin().getAPI().getPlayer(event.getPlayer())
                    .updateStat(getStat("Fish catched"), empty, 1, event.getPlayer().getWorld().getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void kick(PlayerKickEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".timeskicked")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Times kicked")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Times kicked"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void eggThrow(PlayerEggThrowEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".eggsthrown")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Eggs thrown")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Eggs thrown"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        Player ent = (Player) event.getEntity();
        if (ent.hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && ent.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (!this.getPlugin().getSettings().getDisabledStats().contains("Damage taken")) {
            if (plugin.getSettings().isUsingPermissionsForTracking() && !ent.hasPermission(permissionNode) && !ent.hasPermission(permissionNode + ".damagetaken")) {
                return;
            }
            this.getPlugin().getAPI().getPlayer(ent)
                    .getStatData(getStat("Damage taken"), ent.getWorld().getName(), true).addUpdate(empty, event.getDamage());
        }
        if (event.getCause().equals(DamageCause.FIRE)) {
            if (plugin.getSettings().isUsingPermissionsForTracking() && !ent.hasPermission(permissionNode) && !ent.hasPermission(permissionNode + ".onfire")) {
                return;
            }
            if (this.getPlugin().getSettings().getDisabledStats().contains("On fire")) {
                return;
            }
            this.getPlugin().getAPI().getPlayer(ent)
                    .getStatData(getStat("On fire"), ent.getWorld().getName(), true).addUpdate(empty, event.getDamage());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void itemBreak(PlayerItemBreakEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".toolsbroken")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Tools broken")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Tools broken"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void chat(AsyncPlayerChatEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".wordssaid")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Words said")) {
            return;
        }
        int words = event.getMessage().split(" ").length;
        words += words == 0 ? 1 : 0;
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .getStatData(getStat("Words said"), event.getPlayer().getWorld().getName(), true).addUpdate(empty, words);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void entityShoot(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        Player p = (Player) event.getEntity();
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !p.hasPermission(permissionNode) && !p.hasPermission(permissionNode + ".arrows")) || p.hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Arrows")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(p).updateStat(getStat("Arrows"), empty, 1, p.getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void command(PlayerCommandPreprocessEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".commandsdone")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Commands done")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Commands done"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void craftItem(CraftItemEvent event) {
        if (event.getWhoClicked().hasMetadata("NPC")) {
            return;
        }
        if (event.getWhoClicked() instanceof Player) {
            Player p = (Player) event.getWhoClicked();
            if (plugin.getSettings().isUsingPermissionsForTracking() && !p.hasPermission(permissionNode) && !p.hasPermission(permissionNode + ".itemscrafted")) {
                return;
            }
            if (this.getPlugin().getSettings().isIgnoreCreative() && p.getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            if (this.getPlugin().getSettings().getDisabledStats().contains("Items crafted")) {
                return;
            }
            this.getPlugin().getAPI().getPlayer(p).updateStat(getStat("Items crafted"), empty, 1, p.getWorld().getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void eat(FoodLevelChangeEvent event) {
        if (event.getEntity().hasMetadata("NPC")) {
            return;
        }
        if (event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();
            if (event.getFoodLevel() < p.getFoodLevel()) {
                //went down, is not eating
                return;
            }
            if (plugin.getSettings().isUsingPermissionsForTracking() && !(p.hasPermission(permissionNode) || p.hasPermission(permissionNode + ".omnomnom"))) {
                return;
            }
            if (this.getPlugin().getSettings().isIgnoreCreative() && p.getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            if (this.getPlugin().getSettings().getDisabledStats().contains("Omnomnom")) {
                return;
            }
            this.getPlugin().getAPI().getPlayer(p).updateStat(getStat("Omnomnom"), empty, 1, p.getWorld().getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void itemPickup(PlayerPickupItemEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".itempickups")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Itempickups")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Itempickups"), empty, event.getItem().getItemStack().getAmount(), event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void teleports(PlayerTeleportEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".teleports")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Teleports")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer()).updateStat(getStat("Teleports"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void bedEnter(PlayerBedEnterEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".bedenter")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Bedenter")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Bedenter"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void bucketFill(PlayerBucketFillEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".bucketfill")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Bucketfill")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Bucketfill"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void bucketEmpty(PlayerBucketEmptyEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".bucketempty")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Bucketempty")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Bucketempty"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void worldChange(PlayerChangedWorldEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".worldchange")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Worldchange")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Worldchange"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void itemDrop(PlayerDropItemEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".itemdrops")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Itemdrops")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Itemdrops"), empty, event.getItemDrop().getItemStack().getAmount(), event.getPlayer().getWorld().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void shear(PlayerShearEntityEvent event) {
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !event.getPlayer().hasPermission(permissionNode) && !event.getPlayer().hasPermission(permissionNode + ".shear")) || event.getPlayer().hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Shear")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(event.getPlayer())
                .updateStat(getStat("Shear"), empty, 1, event.getPlayer().getWorld().getName());
    }

    @EventHandler
    public void pvp(PlayerDeathEvent event) {
        if (event.getEntity().getKiller() != null) {
            Player killer = event.getEntity().getKiller();
            Player dead = event.getEntity();
            if ((this.getPlugin().getSettings().isIgnoreCreative() && killer.getGameMode().equals(GameMode.CREATIVE))
                    || dead.hasMetadata("NPC")) {
                return;
            }
            if (!killer.hasMetadata("NPC")) {
                StatsPlayer statsKiller = plugin.getAPI().getPlayer(killer);
                ItemStack weapon = killer.getItemInHand();
                if (!plugin.getSettings().isUsingPermissionsForTracking() || killer.hasPermission(permissionNode) || killer.hasPermission(permissionNode + ".pvp")) {
                    /* PVP +1 with id & weapon for killer */
                    statsKiller.getStatData(getStat("pvp"), killer.getWorld().getName(), true).addUpdate(new Object[]{
                        plugin.getAPI().getPlayer(dead).getId(),
                        weapon == null ? "Fists" : (weapon.getType().name().substring(0, 1) + weapon.getType().name().substring(1).toLowerCase().replace("_", " "))
                    }, 1);
                }

                StatData pvpStreakKiller = statsKiller.getStatData(getStat("PVPStreak"), killer.getWorld().getName(), true);
                if (!plugin.getSettings().isUsingPermissionsForTracking() || killer.hasPermission(permissionNode) || killer.hasPermission(permissionNode + ".pvpstreak")) {
                    /* PVPStreak +1 for killer */

                    pvpStreakKiller.setCurrentValue(empty, pvpStreakKiller.getValue(empty) + 1);
                    pvpStreakKiller.forceUpdate(empty);
                }

                if (!plugin.getSettings().isUsingPermissionsForTracking() || killer.hasPermission(permissionNode) || killer.hasPermission(permissionNode + ".pvptopstreak")) {
                    /* Update top streak if nessecary */
                    StatData pvpTopStreak = statsKiller.getStatData(getStat("PVPTopStreak"), killer.getWorld().getName(), true);
                    if (pvpStreakKiller.getValue(empty) > pvpTopStreak.getValue(empty)) {
                        pvpTopStreak.setCurrentValue(empty, pvpStreakKiller.getValue(empty));
                        pvpTopStreak.forceUpdate(empty);
                    }
                }
            }
            StatsPlayer statsDead = plugin.getAPI().getPlayer(dead);
            if (!plugin.getSettings().isUsingPermissionsForTracking() || dead.hasPermission(permissionNode) || dead.hasPermission(permissionNode + ".pvpstreak")) {
                /* Update PVPStreak to 0 for dead */
                StatData pvpStreakDead = statsDead.getStatData(getStat("PVPStreak"), dead.getWorld().getName(), true);
                pvpStreakDead.setCurrentValue(empty, 0);
                pvpStreakDead.forceUpdate(empty);
            }
        }
    }

    @EventHandler
    public void trade(InventoryClickEvent event) {
        if (event.getInventory().getType() != InventoryType.MERCHANT) {
            return;
        }
        if (!event.getSlotType().equals(SlotType.RESULT)) {
            return;
        }
        if (!event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY) && !event.getAction().equals(InventoryAction.PICKUP_ALL)) {
            return;
        }
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getWhoClicked();
        if ((plugin.getSettings().isUsingPermissionsForTracking() && !player.hasPermission(permissionNode) && !player.hasPermission(permissionNode + ".trades")) || player.hasMetadata("NPC")) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (this.getPlugin().getSettings().getDisabledStats().contains("Trades")) {
            return;
        }
        this.getPlugin().getAPI().getPlayer(player)
                .updateStat(getStat("Trades"), empty, 1, player.getWorld().getName());
        //trading
    }

    public Stat getStat(String name) {
        return this.plugin.getStatTypes().get(name);
    }

    public byte getByte(int block, byte def) {
        switch (block) {
            case 23:
            case 26:
            case 27:
            case 28:
            case 29:
            case 33:
            case 50:
            case 53:
            case 54:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 71:
            case 75:
            case 76:
            case 77:
            case 85:
            case 86:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 111:
            case 113:
            case 114:
            case 115:
            case 117:
            case 127:
                return (byte) 0x0;
            default:
                return def;
        }
    }
}
