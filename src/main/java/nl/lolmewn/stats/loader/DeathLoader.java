package nl.lolmewn.stats.loader;

import java.sql.ResultSet;
import java.sql.SQLException;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class DeathLoader extends DataLoader {

    public DeathLoader(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean load(StatsPlayer player, Stat stat, ResultSet set) throws SQLException {
        player.getStatData(stat, set.getString("world"), true).setCurrentValue(new Object[]{set.getString("cause"), set.getBoolean("entity")}, set.getInt("amount"));
        return true;
    }

}
