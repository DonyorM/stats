package nl.lolmewn.stats.saver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.saver.DataSaver;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class BlockSaver extends DataSaver {

    public BlockSaver(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean save(StatsPlayer player, Stat stat, Connection con) throws SQLException {
        for (String world : player.getWorlds()) {
            StatData data = player.getStatData(stat, world, false);
            if (data == null) {
                continue;
            }
            for (Object[] update : data.getUpdateVariables()) {
                double value = stat.getDataType().equals(StatDataType.INCREASING) ? data.getUpdateValue(update, true) : data.getValue(update, true);
                StringBuilder sb = new StringBuilder();
                sb.append("UPDATE ").append(stat.getTable().getName());
                sb.append(" SET amount=amount+?");
                sb.append(" WHERE player_id=? AND blockID=? AND blockData=? AND break=? AND world=? ");
                if (this.getAPI().isCreatingSnapshots()) {
                    sb.append("AND snapshot_name=?");
                }
                PreparedStatement st = con.prepareStatement(sb.toString());
                st.setDouble(1, value);
                st.setInt(2, player.getId());
                st.setObject(3, update[0]);
                st.setObject(4, update[1]);
                st.setInt(5, (Boolean)update[2] ? 1 : 0);
                st.setString(6, world);
                if (this.getAPI().isCreatingSnapshots()) {
                    st.setString(7, "main_snapshot");
                }
                int updatedRows = st.executeUpdate();
                st.close();
                if (updatedRows == 0) {
                    sb = new StringBuilder();
                    sb.append("INSERT INTO ").append(stat.getTable().getName()).append(" (player_id, blockID, blockData, break, world");
                    if (this.getAPI().isCreatingSnapshots()) {
                        sb.append(", snapshot_name");
                    }
                    sb.append(", amount");
                    sb.append(") VALUES (?,?,?,?,?,?");
                    if (this.getAPI().isCreatingSnapshots()) {
                        sb.append(",?");
                    }
                    sb.append(")");
                    st = con.prepareStatement(sb.toString());
                    st.setInt(1, player.getId());
                    st.setObject(2, update[0]);
                    st.setObject(3, update[1]);
                    st.setObject(4, (Boolean)update[2] ? 1 : 0);
                    st.setString(5, world);
                    if (this.getAPI().isCreatingSnapshots()) {
                        st.setString(6, "main_snapshot");
                    }
                    st.setDouble(this.getAPI().isCreatingSnapshots() ? 7 : 6, value);
                    st.executeUpdate();
                    st.close();
                }
            }
        }
        return true;
    }
}
